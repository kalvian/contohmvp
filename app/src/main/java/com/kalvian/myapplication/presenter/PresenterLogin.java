package com.kalvian.myapplication.presenter;

import android.text.TextUtils;

import com.kalvian.myapplication.view.LoginView;

/**
 * Created by root on 07/08/16.
 */
public class PresenterLogin implements LoginPresenter {


    //class untuk prosess data

    LoginView loginView;

    public PresenterLogin(LoginView loginView) {
        this.loginView = loginView;
    }

    @Override
    public void login(String username, String password) {

        if(TextUtils.isEmpty(username) || TextUtils.isEmpty(password)){
            loginView.validationError();
        }else{

            if(username.equals("admin") && password.equals("admin")){
                loginView.loginSucces();

            }else {
                loginView.loginError();
            }
        }

    }
}
