package com.kalvian.myapplication;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.kalvian.myapplication.presenter.LoginPresenter;
import com.kalvian.myapplication.presenter.PresenterLogin;
import com.kalvian.myapplication.view.LoginView;

public class MainActivity extends AppCompatActivity implements LoginView {

    private EditText input_user_name;
    private EditText input_user_password;
    private Button btn_login;
    private PresenterLogin presenterLogin;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        input_user_name=(EditText)findViewById(R.id.input_user_name);
        input_user_password=(EditText)findViewById(R.id.input_user_password);
        btn_login=(Button)findViewById(R.id.btn_login);


        presenterLogin=new PresenterLogin(this);

        btn_login.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                presenterLogin.login(input_user_name.getText().toString(), input_user_password.getText().toString());
            }
        });


    }

    /**
     * Show Toast
     * @param s
     */
    void showToast(String s){
        Toast.makeText(MainActivity.this,s, Toast.LENGTH_SHORT).show();
    }

    @Override
    public void validationError() {
            showToast("Username dan Password is null!");
    }

    @Override
    public void loginError() {
            showToast("Username dan Password mudah ditebak");
    }

    @Override
    public void loginSucces() {
            startActivity(new Intent(getApplicationContext(),HomeActivity.class));
    }
}
