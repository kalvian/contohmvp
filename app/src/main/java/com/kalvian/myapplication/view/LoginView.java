package com.kalvian.myapplication.view;

/**
 * Created by root on 07/08/16.
 */
public interface LoginView {

    /**
     * Menampilkan View ketika error validasi
     */
    void validationError();

    /**
     * Menampilkan error login
     */
    void loginError();


    /**
     * Menampilkan success login
     */
    void loginSucces();


}
