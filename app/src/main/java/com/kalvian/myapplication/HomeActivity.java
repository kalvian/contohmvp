package com.kalvian.myapplication;


import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;

import butterknife.BindView;
import butterknife.ButterKnife;
import rx.Observable;
import rx.functions.Action1;

public class HomeActivity extends AppCompatActivity {

    @BindView(R.id.tMain)
    TextView tMain;

    @BindView(R.id.btn_do_subscribe)
    Button btnDoSubscribe;

    @BindView(R.id.radio_basic)
    RadioButton radio_basic;

    @BindView(R.id.radio_map)
    RadioButton radio_map;

    @BindView(R.id.radio_active)
    RadioGroup radio_active;

    final Observable<String> myObservable=Observable.just("Hello World!");

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home);
        //set bind
        ButterKnife.bind(this);

        btnDoSubscribe.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                doReactiveProgramming();
            }
        });
    }

    private void doReactiveProgramming() {
        int id=radio_active.getCheckedRadioButtonId();
        switch (id) {
            case R.id.radio_basic :
                myObservable.subscribe(new Action1<String>() {
                    @Override
                    public void call(String s) {
                        tMain.setText("Basic");
                    }
                });
             break;

        }
    }
}
